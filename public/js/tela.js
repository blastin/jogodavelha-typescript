class Tela {

    static obterRepresentacaoBolinha() {
        return '<img src="../img/O50x50.png" />';
    }

    static obterRepresentacaoX() {
        return '<img src="../img/X50x50.png" />'
    }

    static elementoPorId(id) {
        return document.getElementById(id);
    }

    static mudarTextoDoElemento(id, texto) {
        const elemento = Tela.elementoPorId(id);
        elemento.innerHTML = texto;
    }

    static realizarJogada(id, representacao) {
        const elemento = Tela.elementoPorId("bt" + id);
        elemento.innerHTML = representacao;
    }

    static obterEvento() {
        return 'status';
    }

    static obterBotaoReiniciar() {
        return Tela.elementoPorId("btReiniciar");
    }

    static jogadorGanhou() {
        const evento = Tela.obterEvento();
        Tela.mudarTextoDoElemento(evento, "você ganhou");
    }

    static jogadorExternoGanhou() {
        const evento = Tela.obterEvento();
        Tela.mudarTextoDoElemento(evento, "você perdeu");
    }

    static empate() {
        const evento = Tela.obterEvento();
        Tela.mudarTextoDoElemento(evento, "empate");
    }

    static jogadaNaoEhPossivel() {
        const evento = Tela.obterEvento();
        Tela.mudarTextoDoElemento(evento, "não foi possível inserir");
    }

    static esperandoJogadaExterna() {
        const evento = Tela.obterEvento();
        Tela.mudarTextoDoElemento(evento, "Esperando jogada do jogador externo");
    }

    static limparEvento() {
        const evento = Tela.obterEvento();
        Tela.mudarTextoDoElemento(evento, "");
    }

    static realizeSuaJogada() {
        const evento = Tela.obterEvento();
        Tela.mudarTextoDoElemento(evento, "realize sua jogada");
    }


    static mostrarBotaoReiniciar() {
        console.log("oi");
        const evento = Tela.obterBotaoReiniciar();
        evento.style.display = "inherit";
    }

    static esconderBotaoReiniciar() {
        const evento = Tela.obterBotaoReiniciar();
        evento.style.display = "none";
    }

    static limparjogadas() {
        for (let jogada = 1; jogada <= 9; jogada++) {
            Tela.realizarJogada(jogada, "");
        }
    }
}