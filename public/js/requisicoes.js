class Requisicoes {

    static postarJogada(jogador, callback) {
        const xhttp = Requisicoes.construirXhttp(callback);
        xhttp.open("POST", Requisicoes.construirLinkParaJogada(), true);
        xhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
        xhttp.setRequestHeader("esperoJogadaAtributo", jogador.esperoJogadaAtributo);
        xhttp.setRequestHeader("jogadaAtributo", jogador.jogadaAtributo);
        xhttp.setRequestHeader("jogoFinalizouAtributo", jogador.jogoFinalizouAtributo);
        xhttp.send();
    }

    static receberJogada(jogador, callback) {
        const xhttp = Requisicoes.construirXhttp(callback);
        xhttp.open("GET", Requisicoes.construirLinkParaJogada(), true);
        xhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
        xhttp.setRequestHeader("esperoJogadaAtributo", jogador.esperoJogadaAtributo);
        xhttp.setRequestHeader("jogoFinalizouAtributo", jogador.jogoFinalizouAtributo);
        xhttp.send();
    }

    static reiniciarJogo(jogador, callback) {
        const xhttp = Requisicoes.construirXhttp(callback);
        xhttp.open("GET", Requisicoes.construirLinkParaReiniciarJogo(), true);
        xhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
        xhttp.send();
    }

    static construirXhttp(callback) {
        const xhttp = new XMLHttpRequest();
        xhttp.onreadystatechange = function () {
            if (this.readyState === 4 && this.status === 200) {
                callback(JSON.parse(this.response));
            }
        };
        return xhttp;
    }

    static construirLinkParaJogada() {
        return "/jogada/" + Storage.receberSessao() + "/" + Storage.receberCliente();
    }

    static construirLinkParaReiniciarJogo() {
        return "/reiniciar-jogo/" + Storage.receberSessao() + "/" + Storage.receberCliente();
    }
}