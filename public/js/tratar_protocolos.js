const Protocolos = {
    JOGO_FINALIZOU: 71007, // JogoFinalizouException
    JOGADA_REALIZADA: 71010,
    JOGADA_RECEBIDA: 71011,
    SEM_JOGADA_NOVA_EM_JOGO_DA_VELHA: 71014, // SemJogadaNovasException
    JOGADOR_NAO_PODE_RECEBER_A_MESMA_JOGADA_QUE_POSTOU: 71015, // JogadorRealizouUltimaJogadaException
    JOGO_REINICIADO: 71018,
};

class TratarProtocolo {

    static jogada(response, callback) {
        if (response.status === Protocolos.JOGADA_REALIZADA) {
            callback(true);
        } else {
            callback(false);
        }
    }

    static receber(response, callback) {
        if (response.status === Protocolos.JOGADA_RECEBIDA) {
            callback(response);
        } else if (response.status === Protocolos.SEM_JOGADA_NOVA_EM_JOGO_DA_VELHA) {
            callback(response);
        } else if (response.status === Protocolos.JOGO_FINALIZOU) {
            callback(response);
        } else if (response.status === Protocolos.JOGADOR_NAO_PODE_RECEBER_A_MESMA_JOGADA_QUE_POSTOU) {
            callback(response);
        } else {
            console.error(response.status);
        }
    }

    static reiniciar(response, callback) {
        if (response.status === Protocolos.JOGO_REINICIADO && response.jogador !== undefined) {
            callback(response);
        } else {
            console.error(response.status);
        }
    }
}