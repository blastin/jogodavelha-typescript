class Brasao {

    constructor(representacao, valor) {
        this.r = representacao;
        this.v = valor;
    }

    representacao() {
        return this.r;
    }

    valor() {
        return this.v;
    }

}
