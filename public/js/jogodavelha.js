class JogoFinalizouException extends Error {
}

class NaoEhPossivelRealizarJogadaException extends Error {
}

class JogadorEsperaJogadaException extends Error {
}

class JogoDaVelha {

    static armazenarInformacoesDeAutenticacao(response) {
        Storage.armazenarCliente(response.cliente);
        Storage.armazenarSessao(response.sessao);
    }

    constructor(response) {
        this.jogador = new Jogador(this, response.jogador);
        this.tabuleiro = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0];
        JogoDaVelha.armazenarInformacoesDeAutenticacao(response);
    }

    ganhador() {

        function condicaoTernaria(A, B, C) {
            return A & B & C;
        }

        const tabuleiro = this.tabuleiro;

        if (tabuleiro[0] === 9) {
            return 0;
        }
        else if (condicaoTernaria(tabuleiro[1], tabuleiro[2], tabuleiro[3]) > 0) {
            return tabuleiro[1];
        } else if (condicaoTernaria(tabuleiro[4], tabuleiro[5], tabuleiro[6]) > 0) {
            return tabuleiro[4];
        } else if (condicaoTernaria(tabuleiro[7], tabuleiro[8], tabuleiro[9]) > 0) {
            return tabuleiro[7];
        } else if (condicaoTernaria(tabuleiro[1], tabuleiro[4], tabuleiro[7]) > 0) {
            return tabuleiro[1];
        } else if (condicaoTernaria(tabuleiro[2], tabuleiro[5], tabuleiro[8]) > 0) {
            return tabuleiro[2];
        } else if (condicaoTernaria(tabuleiro[3], tabuleiro[6], tabuleiro[9]) > 0) {
            return tabuleiro[3];
        } else if (condicaoTernaria(tabuleiro[1], tabuleiro[5], tabuleiro[9]) > 0) {
            return tabuleiro[1];
        } else if (condicaoTernaria(tabuleiro[3], tabuleiro[5], tabuleiro[7]) > 0) {
            return tabuleiro[3];
        } else {
            return 0;
        }

    }

    tratarJogo(possivelGanhador) {

        if (possivelGanhador === 1) {
            Tela.jogadorGanhou();
            Tela.mostrarBotaoReiniciar();
            this.jogador.jogoFinalizouAtributo = true;
        } else if (possivelGanhador === 2) {
            Tela.jogadorExternoGanhou();
            Tela.mostrarBotaoReiniciar();
            this.jogador.jogoFinalizouAtributo = true;
        } else if (this.tabuleiro[0] === 9) {
            Tela.empate();
            Tela.mostrarBotaoReiniciar();
            this.jogador.jogoFinalizouAtributo = true;
        }
    }

    jogada(valor) {
        try {
            this.validarContinuidadeDoJogo();
            this.validarJogada(valor);
            this.jogador.realizarJogada(valor);
        } catch (e) {
            if (e instanceof NaoEhPossivelRealizarJogadaException) {
                Tela.jogadaNaoEhPossivel();
            } else if (e instanceof JogoFinalizouException) {
                this.tratarJogo(this.ganhador());
            } else if (e instanceof JogadorEsperaJogadaException) {
            }
        }

    }

    inserirJogadaEmTabuleiro(valor, valorBrasao) {
        this.tabuleiro[valor] = valorBrasao;
        this.tabuleiro[0] += 1;
        this.tratarJogo(this.ganhador());
    }

    validarJogada(id) {
        if (!(this.tabuleiro[id] === 0)) {
            throw new NaoEhPossivelRealizarJogadaException();
        }
        if (this.jogador.esperoJogadaAtributo) {
            throw new JogadorEsperaJogadaException();
        }
    }

    validarContinuidadeDoJogo() {
        if (this.jogador.jogoFinalizouAtributo) {
            throw new JogoFinalizouException()
        }
    }

    reiniciar() {
        this.tabuleiro = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0];
        this.jogador.reiniciarJogo();
        Tela.limparjogadas();
        Tela.esconderBotaoReiniciar();
    }
}
