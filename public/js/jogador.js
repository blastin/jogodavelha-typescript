class Jogador {

    constructor(jogodavelha, jogador) {
        this.jogodavelha = jogodavelha;
        this.brasao = new Brasao(Tela.obterRepresentacaoX(), 1);
        this.brasaoJogadaExterna = new Brasao(Tela.obterRepresentacaoBolinha(), 2);
        this.novoJogo(jogador);
    }

    novoJogo(jogador) {
        this.jogadaAtributo = jogador.jogadaAtributo;
        this.esperoJogadaAtributo = jogador.esperoJogadaAtributo;
        this.jogoFinalizouAtributo = jogador.jogoFinalizouAtributo;
        if (this.esperoJogadaAtributo === true) {
            this.receberJogada();
        } else {
            Tela.realizeSuaJogada();
        }
    }

    realizarJogada(valor) {

        this.jogadaAtributo = valor;

        const self = this;

        Requisicoes.postarJogada(self, function (response) {
            TratarProtocolo.jogada(response, function (response) {
                if (response === true) {
                    self.esperoJogadaAtributo = true;
                    Tela.realizarJogada(valor, self.brasao.representacao());
                    self.jogodavelha.inserirJogadaEmTabuleiro(valor, self.brasao.valor());
                    if (!self.jogoFinalizouAtributo) {
                        self.receberJogada();
                    }
                } else {
                    console.error("Error servidor");
                }
            });
        });
    }

    receberJogada() {

        const self = this;

        let timeout;

        Tela.esperandoJogadaExterna();

        function receberJogada() {
            Requisicoes.receberJogada(self, function (response) {
                TratarProtocolo.receber(response, function (response) {
                    if (response.status === Protocolos.JOGADA_RECEBIDA ||
                        response.status === Protocolos.JOGO_FINALIZOU) {
                        self.esperoJogadaAtributo = false;
                        Tela.realizarJogada(response.jogadaAtributo, self.brasaoJogadaExterna.representacao());
                        Tela.realizeSuaJogada();
                        self.jogodavelha.inserirJogadaEmTabuleiro(response.jogadaAtributo, self.brasaoJogadaExterna.valor());
                        clearTimeout(timeout);
                    }
                });
            });
            timeout = setTimeout(receberJogada, 3000);
        }

        receberJogada();
    }

    reiniciarJogo() {
        const self = this;
        Requisicoes.reiniciarJogo(self, function (response) {
            TratarProtocolo.reiniciar(response, function (response) {
                if (response.status === Protocolos.JOGO_REINICIADO) {
                    self.novoJogo(response.jogador);
                }
            });
        });
    }

    valorBrasao() {
        return this.brasao.valor();
    }

}
