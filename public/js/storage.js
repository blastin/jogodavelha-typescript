class Storage {

    static armazenar(chave, valor) {
        sessionStorage.setItem(chave, valor);
    }

    static receber(chave) {
        return sessionStorage.getItem(chave);
    }

    static armazenarCliente(cliente) {
        Storage.armazenar('cliente', cliente);
    }

    static armazenarSessao(sessao) {
        Storage.armazenar('sessao', sessao);
    }

    static receberCliente() {
        return Storage.receber('cliente');
    }

    static receberSessao() {
        return Storage.receber('sessao');
    }

}