import Sessao from "./sessao";

interface ISessao {
    chave: string;
    sessao: Sessao;
}

export default ISessao;
