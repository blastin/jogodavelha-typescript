import Chave from "../models/chave";
import Cliente from "../models/cliente";
import ICompartilhamento from "../models/icompartilhamento";
import ClienteNaoDeveConterChaveQuandoInseridaEmSessaoException
    from "./clienteNaoDeveConterChaveQuandoInseridaEmSessaoException";
import ICliente from "./ICliente";
import JahExisteClienteException from "./jahExisteClienteException";
import RepositorioChave from "./repositorio.chave";
import RepositorioDeChavesDeSessoesUndefinedException from "./repositorioDeChavesDeSessoesUndefinedException";

class Sessao {

    private readonly chaveAtt: Chave;
    private readonly compartilhamentoPorSessaoAtt: ICompartilhamento;
    private clientes: ICliente[];
    private repositorioChaves: RepositorioChave;
    private repositorioDeChavesDeSessoesAtt: RepositorioChave | undefined;

    constructor(compartilhamentoPorSessao: ICompartilhamento) {
        this.chaveAtt = new Chave();
        this.repositorioChaves = new RepositorioChave();
        this.clientes = [];
        this.compartilhamentoPorSessaoAtt = compartilhamentoPorSessao;
        this.repositorioDeChavesDeSessoesAtt = undefined;
    }

    set repositorioDeChavesDeSessoes(value: RepositorioChave) {
        this.repositorioDeChavesDeSessoesAtt = value;
    }

    get chave(): Chave {
        return this.chaveAtt;
    }

    get compartilhamentoPorSessao(): ICompartilhamento {
        return this.compartilhamentoPorSessaoAtt;
    }

    public gerarChaveDeSessao(): void {
        this.chaveAtt.gerarChave();
    }

    public inserirCliente(cliente: Cliente): void {
        if (this.existeCliente(cliente)) {
            throw new JahExisteClienteException();
        }
        if (cliente.chave.chaveGerada()) {
            throw new ClienteNaoDeveConterChaveQuandoInseridaEmSessaoException();
        }
        this.gerarChaveDeCliente(cliente);
        const object: ICliente = {
            chave: cliente.chave.toString(),
            cliente,
        };
        cliente.start(this.compartilhamentoPorSessao);
        this.repositorioChaves.inserirChave(cliente.chave);
        this.clientes.push(object);
    }

    public encontrarClientePorChave(chave: Chave): Cliente | undefined {
        const icliente = this.clientes.find((object) => object.chave === chave.toString());
        if (icliente !== undefined) {
            return icliente.cliente;
        }
        return undefined;
    }

    public existeCliente(cliente: Cliente): boolean {
        return this.clientes.find((object) => object.cliente === cliente) !== undefined;
    }

    private gerarChaveDeCliente(cliente: Cliente): void {
        if (this.repositorioDeChavesDeSessoesAtt === undefined) {
            throw new RepositorioDeChavesDeSessoesUndefinedException();
        }
        do {
            cliente.gerarChaveDeCliente();
        } while (this.repositorioChaves.existeChave(cliente.chave) ||
        this.repositorioDeChavesDeSessoesAtt.existeChave(cliente.chave));
    }
}

export default Sessao;
