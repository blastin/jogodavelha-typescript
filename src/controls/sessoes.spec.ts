import {expect} from "chai";
import "mocha";
import Chave from "../models/chave";
import JogodaVelhaImpl from "../models/JogodaVelhaImpl";
import JahExisteSessaoException from "./jahExisteSessaoException";
import Sessao from "./sessao";
import SessaoNaoDeveConterChaveQuandoInseridaEmSessoesException
    from "./sessaoNaoDeveConterChaveQuandoInseridaEmSessoesException";
import Sessoes from "./sessoes";

describe("tratar sessoes", () => {
    const sessoes = new Sessoes();

    it("deve inserirChave uma sessao", () => {
        const sessao = new Sessao(new JogodaVelhaImpl());
        sessoes.inserirSessao(sessao);
        expect(sessoes.existeSessao(sessao)).to.be.true;
    });

    it("deve inserir três sessoes", () => {
        const A = new Sessao(new JogodaVelhaImpl());
        sessoes.inserirSessao(A);
        const B = new Sessao(new JogodaVelhaImpl());
        sessoes.inserirSessao(B);
        const C = new Sessao(new JogodaVelhaImpl());
        sessoes.inserirSessao(C);
        expect(sessoes.existeSessao(A)).to.be.true;
        expect(sessoes.existeSessao(B)).to.be.true;
        expect(sessoes.existeSessao(C)).to.be.true;
        expect(sessoes.existeSessao(new Sessao(new JogodaVelhaImpl()))).to.be.false;
    });

    it("deve existir sessao e gerar exceção", () => {
        const A = new Sessao(new JogodaVelhaImpl());
        sessoes.inserirSessao(A);
        expect(() => sessoes.inserirSessao(A)).to.throw(JahExisteSessaoException);
    });

    it("sessao contem chave e deve gerar exceção", () => {
        const A = new Sessao(new JogodaVelhaImpl());
        A.gerarChaveDeSessao();
        expect(() => sessoes.inserirSessao(A)).to.throw(SessaoNaoDeveConterChaveQuandoInseridaEmSessoesException);
    });

    it("encontrar sessao por chave", () => {
        const sessao = new Sessao(new JogodaVelhaImpl());
        sessoes.inserirSessao(sessao);
        expect(sessoes.existeSessao(sessao)).to.be.true;
        const chave = sessao.chave;
        expect(sessoes.encontrarSessaoPorChave(chave)).to.equal(sessao);
    });

    it("nao deve encontrar sessao por chave", () => {
        const sessao = new Sessao(new JogodaVelhaImpl());
        sessoes.inserirSessao(sessao);
        expect(sessoes.existeSessao(sessao)).to.be.true;
        const chave = new Chave();
        chave.associarValor("0");
        expect(sessoes.encontrarSessaoPorChave(chave)).to.equal(undefined);
    });
});
