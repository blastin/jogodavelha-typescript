import Chave from "../models/chave";
import ISessao from "./ISessao";
import JahExisteSessaoException from "./jahExisteSessaoException";
import RepositorioChave from "./repositorio.chave";
import Sessao from "./sessao";
import SessaoNaoDeveConterChaveQuandoInseridaEmSessoesException
    from "./sessaoNaoDeveConterChaveQuandoInseridaEmSessoesException";

class Sessoes {

    private readonly repositorioChaves: RepositorioChave;
    private sessoes: ISessao[];

    constructor() {
        this.repositorioChaves = new RepositorioChave();
        this.sessoes = [];
    }

    public inserirSessao(sessao: Sessao): void {
        if (this.existeSessao(sessao)) {
            throw new JahExisteSessaoException();
        }
        if (sessao.chave.chaveGerada()) {
            throw new SessaoNaoDeveConterChaveQuandoInseridaEmSessoesException();
        }
        this.gerarChaveDeSessao(sessao);
        const object: ISessao = {
            chave: sessao.chave.toString(),
            sessao,
        };
        this.sessoes.push(object);
        this.repositorioChaves.inserirChave(sessao.chave);
        sessao.repositorioDeChavesDeSessoes = this.repositorioChaves;
    }

    public encontrarSessaoPorChave(chave: Chave): Sessao | undefined {
        const isessao = this.sessoes.find((object) => object.chave === chave.toString());
        if (isessao !== undefined) {
            return isessao.sessao;
        }
        return undefined;
    }

    public existeSessao(sessao: Sessao): boolean {
        return this.sessoes.find((object) => object.sessao === sessao) !== undefined;
    }

    private gerarChaveDeSessao(sessao: Sessao): void {
        do {
            sessao.gerarChaveDeSessao();
        } while (this.repositorioChaves.existeChave(sessao.chave));
    }
}

export default Sessoes;
