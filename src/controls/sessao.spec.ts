import {expect} from "chai";
import "mocha";
import Cliente from "../models/cliente";
import JogodaVelhaImpl from "../models/JogodaVelhaImpl";
import ClienteNaoDeveConterChaveQuandoInseridaEmSessaoException
    from "./clienteNaoDeveConterChaveQuandoInseridaEmSessaoException";
import JahExisteClienteException from "./jahExisteClienteException";
import Sessao from "./sessao";
import Sessoes from "./sessoes";

describe("tratar sessao e cliente", () => {
    it("deve inserir cliente", () => {
        const sessoes = new Sessoes();
        const sessao = new Sessao(new JogodaVelhaImpl());
        sessoes.inserirSessao(sessao);
        const cliente = new Cliente();
        sessao.inserirCliente(cliente);
        expect(sessao.existeCliente(cliente)).to.be.true;
    });
    it("deve tentar inserir cliente ja inserido", () => {
        const sessoes = new Sessoes();
        const sessao = new Sessao(new JogodaVelhaImpl());
        sessoes.inserirSessao(sessao);
        const cliente = new Cliente();
        sessao.inserirCliente(cliente);
        expect(() => sessao.inserirCliente(cliente)).to.throw(JahExisteClienteException);
    });
    it("deve tentar inserir cliente ja com chave", () => {
        const sessoes = new Sessoes();
        const sessao = new Sessao(new JogodaVelhaImpl());
        sessoes.inserirSessao(sessao);
        const cliente = new Cliente();
        cliente.gerarChaveDeCliente();
        expect(() => sessao.inserirCliente(cliente)).to.throw(ClienteNaoDeveConterChaveQuandoInseridaEmSessaoException);
    });
    it("encontrar cliente por chave", () => {
        const sessoes = new Sessoes();
        const sessao = new Sessao(new JogodaVelhaImpl());
        sessoes.inserirSessao(sessao);
        const cliente = new Cliente();
        sessao.inserirCliente(cliente);
        const chave = cliente.chave;
        expect(sessao.encontrarClientePorChave(chave)).to.equal(cliente);
    });
});
