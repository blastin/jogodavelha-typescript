import Cliente from "../models/cliente";

interface ICliente {
    chave: string;
    cliente: Cliente;
}

export default ICliente;
