import Chave from "../models/chave";
import ChaveNaoexisteException from "./chave.naoexiste.exception";

class RepositorioChave {
    private chaves: Chave[];

    constructor() {
        this.chaves = [];
    }

    public existeChave(chave: Chave): boolean {
        return this.chaves.find((c) => c.equal(chave)) !== undefined;
    }

    public inserirChave(chave: Chave): void {
        if (this.existeChave(chave)) {
            throw new ChaveNaoexisteException();
        }
        this.chaves.push(chave);
    }

}

export default RepositorioChave;
