import nodemailer from "nodemailer";
import EmailErrorException from "./emailErrorException";
import Mail = require("nodemailer/lib/mailer");

class EmailErrors {

    private transporter: Mail;
    private existeContaEmail: boolean;

    constructor() {

        this.existeContaEmail = process.env.PASSWORD_GMAIL !== undefined && process.env.USER_GMAIL !== undefined;

        this.transporter = nodemailer.createTransport({
            auth: {
                pass: process.env.PASSWORD_GMAIL,
                user: process.env.USER_GMAIL,
            },
            host: "smtp.gmail.com",
            logger: true,
            port: 465,
            secure: true,
            service: "gmail",
            socketTimeout: 5000,
        });

    }

    public enviarMensagem(mensagem: string): void {
        if (this.existeContaEmail) {
            const mailOptions: Mail.Options = {
                from: process.env.USER_GMAIL,
                subject: "ERROR:" + Date.now(),
                text: mensagem,
                to: process.env.USER_GMAIL,
            };
            const promise = new Promise((resolve, reject) => {
                this.transporter.sendMail(mailOptions, (error, info) => {
                    if (error !== null) {
                        throw new EmailErrorException(error.message);
                    }
                });
            });
        } else {
            console.log("não é possivel enviar email pois conta não foi informada");
        }
    }
}

export default EmailErrors;
