
enum Protocolos {
    CHAVE_ASSOCIACAO_DE_VALOR_INDEFINIDO = 71000, // AssociacaoDeValorIndefinidoException
    SESSAO_NAO_ENCONTRADA = 71001, // SessaoNaoEncontradaException
    QUANTIDADE_DE_JOGADORES_EXCEDEU_LIMITE = 71002, // QuantidadeDeJogadoresMaiorDoQuePermitidoException
    INTERNAL_ERROR = 71003,
    CLIENTE_NAO_ENCONTRADO = 71004, // ClienteNaoEncontradoException
    JOGADOR_NAO_DEFINIDO = 71005, // JogadorNaoDefinidoException
    JOGADOR_INVALIDO = 71006, // JogadorInvalidoException
    JOGO_FINALIZOU = 71007, // JogoFinalizouException
    JOGADOR_JAH_REALIZOU_JOGADA = 71008, // JogadorRealizouUltimaJogadaException
    JOGADOR_ESPERA_JOGADA = 71009, // JogadorEsperaJogadaException
    JOGADA_REALIZADA = 71010,
    JOGADA_RECEBIDA = 71011,
    JOGADOR_JAH_RECEBEU_JOGADA = 71012, // JogadorRecebeuUltimaJogadaException
    JOGADOR_NAO_ESPERA_JOGADA = 71013, // JogadorNaoEsperaJogadaException
    SEM_JOGADA_NOVA_EM_JOGO_DA_VELHA = 71014, // SemJogadaNovasException
    JOGADOR_NAO_PODE_RECEBER_A_MESMA_JOGADA_QUE_POSTOU = 71015, // JogadorRealizouUltimaJogadaException
    JOGO_NAO_FINALIZOU = 71016, // JogoNaoFinalizouException
    JOGADOR_NAO_AUTORIZADO = 71017, // JogadorNaoPertenceAEsseJogoDaVelhaException
    JOGO_REINICIADO = 71018,
}
export default Protocolos;
