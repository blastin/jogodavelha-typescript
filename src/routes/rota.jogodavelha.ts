import dotenv from "dotenv";
import {NextFunction, Request, Response, Router} from "express";
import EmailErrors from "../controls/email.errors";
import Sessao from "../controls/sessao";
import Sessoes from "../controls/sessoes";
import AssociacaoDeValorIndefinidoException from "../models/associacaoDeValorIndefinidoException";
import Chave from "../models/chave";
import Cliente from "../models/cliente";
import JogadorEsperaJogadaException from "../models/JogadorEsperaJogadaException";
import Jogadorimpl from "../models/jogadorimpl";
import JogadorInvalidoException from "../models/jogadorInvalidoException";
import JogadorNaoEsperaJogadaException from "../models/JogadorNaoEsperaJogadaException";
import JogadorRealizouUltimaJogadaException from "../models/JogadorRealizouUltimaJogadaException";
import JogadorRecebeuUltimaJogadaException from "../models/JogadorRecebeuUltimaJogadaException";
import JogodaVelhaImpl from "../models/JogodaVelhaImpl";
import JogoFinalizouException from "../models/JogoFinalizouException";
import JogoNaoFinalizouException from "../models/JogoNaoFinalizouException";
import IObjeto from "../models/objeto";
import QuantidadeDeJogadoresMaiorDoQuePermitidoException
    from "../models/QuantidadeDeJogadoresMaiorDoQuePermitidoException";
import SemJogadaNovasException from "../models/SemJogadaNovasException";
import ClienteNaoEncontradoException from "./clienteNaoEncontradoException";
import JogadorNaoDefinidoException from "./jogadorNaoDefinidoException";
import Protocolos from "./protocolos";
import SessaoNaoEncontradaException from "./sessaoNaoEncontradaException";
import JogadorNaoPertenceAEsseJogoDaVelhaException from "../models/JogadorNaoPertenceAEsseJogoDaVelhaException";

dotenv.config();
const sessoes: Sessoes = new Sessoes();
const emailError: EmailErrors = new EmailErrors();
const router: Router = Router();

class RotaJogodavelha {

    public static servidor(req: Request, res: Response, next: NextFunction) {
        res.render("index", {title: "Servidor", status: "Servidor Online"});
    }

    public static novoJogo(req: Request, res: Response, next: NextFunction) {
        const sessao = new Sessao(new JogodaVelhaImpl());
        sessoes.inserirSessao(sessao);
        const cliente = new Cliente();
        sessao.inserirCliente(cliente);
        const response: IObjeto = {
            cliente: cliente.chave.toString(),
            jogador: cliente.informacoes as Jogadorimpl,
            sessao: sessao.chave.toString(),
        };
        res.render("jogodavelha", {title: "Jogo Da Velha", adversario: false, response});
    }

    public static iniciarJogo(req: Request, res: Response, next: NextFunction) {
        try {
            const sessao = RotaJogodavelha.obterSessaoDeRequisicao(req);
            res.render("iniciarjogo",
                {title: "Iniciar Jogo Da Velha", response: {sessao: sessao.chave.toString(), router: "novo-jogo"}});
        } catch (e) {
            if (e instanceof AssociacaoDeValorIndefinidoException) {
                res.send(RotaJogodavelha.statusPorProtocolo(Protocolos.CHAVE_ASSOCIACAO_DE_VALOR_INDEFINIDO));
            } else if (e instanceof SessaoNaoEncontradaException) {
                res.send(RotaJogodavelha.statusPorProtocolo(Protocolos.SESSAO_NAO_ENCONTRADA));
            }
        }
    }

    public static iniciarJogoPorJogadorAdversario(req: Request, res: Response, next: NextFunction) {
        try {
            const sessao = RotaJogodavelha.obterSessaoDeRequisicao(req);
            const cliente = new Cliente();
            sessao.inserirCliente(cliente);
            const jogador = cliente.informacoes as Jogadorimpl;
            const response: IObjeto = {
                cliente: cliente.chave.toString(),
                jogador: cliente.informacoes as Jogadorimpl,
                sessao: sessao.chave.toString(),
            };
            res.render("jogodavelha", {title: "Jogo Da Velha", adversario: true, response});
        } catch (e) {
            if (e instanceof AssociacaoDeValorIndefinidoException) {
                res.send(RotaJogodavelha.statusPorProtocolo(Protocolos.CHAVE_ASSOCIACAO_DE_VALOR_INDEFINIDO));
            } else if (e instanceof SessaoNaoEncontradaException) {
                res.send(RotaJogodavelha.statusPorProtocolo(Protocolos.SESSAO_NAO_ENCONTRADA));
            } else if (e instanceof QuantidadeDeJogadoresMaiorDoQuePermitidoException) {
                res.send(RotaJogodavelha.statusPorProtocolo(Protocolos.QUANTIDADE_DE_JOGADORES_EXCEDEU_LIMITE));
            } else {
                res.send(RotaJogodavelha.statusPorProtocolo(Protocolos.INTERNAL_ERROR));
                emailError.enviarMensagem(e.message);
            }
        }
    }

    public static postarJogada(req: Request, res: Response, next: NextFunction) {
        try {
            const sessao = RotaJogodavelha.obterSessaoDeRequisicao(req);
            const cliente = RotaJogodavelha.obterClienteDeRequisicao(req, sessao);
            const jogadorRequisicao = RotaJogodavelha.construirJogadorPorRequisicao({
                esperoJogadaAtributo: req.headers.esperojogadaatributo,
                jogadaAtributo: req.headers.jogadaatributo,
                jogoFinalizouAtributo: req.headers.jogofinalizouatributo,
            });
            const jogador: Jogadorimpl = cliente.informacoes as Jogadorimpl;
            jogador.validarJogadorJogada(jogadorRequisicao);
            const jogodavelha = sessao.compartilhamentoPorSessao as JogodaVelhaImpl;
            jogodavelha.publicarJogada(jogador, cliente.chave);
            res.send(RotaJogodavelha.statusPorProtocolo(Protocolos.JOGADA_REALIZADA));
        } catch (e) {
            if (e instanceof AssociacaoDeValorIndefinidoException) {
                res.send(RotaJogodavelha.statusPorProtocolo(Protocolos.CHAVE_ASSOCIACAO_DE_VALOR_INDEFINIDO));
            } else if (e instanceof SessaoNaoEncontradaException) {
                res.send(RotaJogodavelha.statusPorProtocolo(Protocolos.SESSAO_NAO_ENCONTRADA));
            } else if (e instanceof ClienteNaoEncontradoException) {
                res.send(RotaJogodavelha.statusPorProtocolo(Protocolos.CLIENTE_NAO_ENCONTRADO));
            } else if (e instanceof JogadorNaoDefinidoException) {
                res.send(RotaJogodavelha.statusPorProtocolo(Protocolos.JOGADOR_NAO_DEFINIDO));
            } else if (e instanceof JogadorInvalidoException) {
                res.send(RotaJogodavelha.statusPorProtocolo(Protocolos.JOGADOR_INVALIDO));
            } else if (e instanceof JogadorNaoPertenceAEsseJogoDaVelhaException) {
                res.send(RotaJogodavelha.statusPorProtocolo(Protocolos.JOGADOR_NAO_AUTORIZADO));
            } else if (e instanceof JogoFinalizouException) {
                res.send(RotaJogodavelha.statusPorProtocolo(Protocolos.JOGO_FINALIZOU));
            } else if (e instanceof JogadorRealizouUltimaJogadaException) {
                res.send(RotaJogodavelha.statusPorProtocolo(Protocolos.JOGADOR_JAH_REALIZOU_JOGADA));
            } else if (e instanceof JogadorEsperaJogadaException) {
                res.send(RotaJogodavelha.statusPorProtocolo(Protocolos.JOGADOR_ESPERA_JOGADA));
            } else {
                res.send(RotaJogodavelha.statusPorProtocolo(Protocolos.INTERNAL_ERROR));
                emailError.enviarMensagem(e.message);
                throw e;
            }
        }
    }

    public static receberJogada(req: Request, res: Response, next: NextFunction) {
        let jogador: Jogadorimpl | undefined;
        try {
            const sessao = RotaJogodavelha.obterSessaoDeRequisicao(req);
            const cliente = RotaJogodavelha.obterClienteDeRequisicao(req, sessao);
            const jogadorRequisicao = RotaJogodavelha.construirJogadorPorRequisicao({
                esperoJogadaAtributo: req.headers.esperojogadaatributo,
                jogoFinalizouAtributo: req.headers.jogofinalizouatributo,
            });
            jogador = cliente.informacoes as Jogadorimpl;
            jogador.validarJogadorRecebimento(jogadorRequisicao);
            const jogodavelha = sessao.compartilhamentoPorSessao as JogodaVelhaImpl;
            jogodavelha.receberJogada(jogador, cliente.chave);
            res.send(Object.assign(RotaJogodavelha.statusPorProtocolo(Protocolos.JOGADA_RECEBIDA), jogador));
        } catch (e) {
            if (e instanceof AssociacaoDeValorIndefinidoException) {
                res.send(RotaJogodavelha.statusPorProtocolo(Protocolos.CHAVE_ASSOCIACAO_DE_VALOR_INDEFINIDO));
            } else if (e instanceof SessaoNaoEncontradaException) {
                res.send(RotaJogodavelha.statusPorProtocolo(Protocolos.SESSAO_NAO_ENCONTRADA));
            } else if (e instanceof ClienteNaoEncontradoException) {
                res.send(RotaJogodavelha.statusPorProtocolo(Protocolos.CLIENTE_NAO_ENCONTRADO));
            } else if (e instanceof JogadorNaoDefinidoException) {
                res.send(RotaJogodavelha.statusPorProtocolo(Protocolos.JOGADOR_NAO_DEFINIDO));
            } else if (e instanceof JogadorInvalidoException) {
                res.send(RotaJogodavelha.statusPorProtocolo(Protocolos.JOGADOR_INVALIDO));
            } else if (e instanceof JogadorNaoPertenceAEsseJogoDaVelhaException) {
                res.send(RotaJogodavelha.statusPorProtocolo(Protocolos.JOGADOR_NAO_AUTORIZADO));
            } else if (e instanceof JogoFinalizouException) {
                res.send(Object.assign(RotaJogodavelha.statusPorProtocolo(Protocolos.JOGO_FINALIZOU), jogador));
            } else if (e instanceof JogadorRecebeuUltimaJogadaException) {
                res.send(RotaJogodavelha.statusPorProtocolo(Protocolos.JOGADOR_JAH_RECEBEU_JOGADA));
            } else if (e instanceof JogadorNaoEsperaJogadaException) {
                res.send(RotaJogodavelha.statusPorProtocolo(Protocolos.JOGADOR_NAO_ESPERA_JOGADA));
            } else if (e instanceof JogadorRealizouUltimaJogadaException) {
                res.send(RotaJogodavelha.statusPorProtocolo(Protocolos.JOGADOR_NAO_PODE_RECEBER_A_MESMA_JOGADA_QUE_POSTOU));
            } else if (e instanceof SemJogadaNovasException) {
                res.send(RotaJogodavelha.statusPorProtocolo(Protocolos.SEM_JOGADA_NOVA_EM_JOGO_DA_VELHA));
            } else {
                res.send(RotaJogodavelha.statusPorProtocolo(Protocolos.INTERNAL_ERROR));
                emailError.enviarMensagem(e.message);
            }
        }
    }

    public static reiniciarJogo(req: Request, res: Response, next: NextFunction) {
        try {
            const sessao = RotaJogodavelha.obterSessaoDeRequisicao(req);
            const cliente = RotaJogodavelha.obterClienteDeRequisicao(req, sessao);
            const jogador = cliente.informacoes as Jogadorimpl;
            const jogodavelha = sessao.compartilhamentoPorSessao as JogodaVelhaImpl;
            jogodavelha.reiniciarJogo(jogador, cliente.chave);
            res.send(Object.assign(RotaJogodavelha.statusPorProtocolo(Protocolos.JOGO_REINICIADO), jogador));
        } catch (e) {

            if (e instanceof AssociacaoDeValorIndefinidoException) {
                res.send(RotaJogodavelha.statusPorProtocolo(Protocolos.CHAVE_ASSOCIACAO_DE_VALOR_INDEFINIDO));
            } else if (e instanceof SessaoNaoEncontradaException) {
                res.send(RotaJogodavelha.statusPorProtocolo(Protocolos.SESSAO_NAO_ENCONTRADA));
            } else if (e instanceof ClienteNaoEncontradoException) {
                res.send(RotaJogodavelha.statusPorProtocolo(Protocolos.CLIENTE_NAO_ENCONTRADO));
            } else if (e instanceof JogadorNaoPertenceAEsseJogoDaVelhaException) {
                res.send(RotaJogodavelha.statusPorProtocolo(Protocolos.JOGADOR_NAO_AUTORIZADO));
            } else if (e instanceof JogoNaoFinalizouException) {
                res.send(RotaJogodavelha.statusPorProtocolo(Protocolos.JOGO_NAO_FINALIZOU));
            }
        }
    }

    private static construirJogadorPorRequisicao(parameters: any): Jogadorimpl {
        if (parameters.esperoJogadaAtributo === undefined || parameters.jogoFinalizouAtributo === undefined) {
            throw new JogadorNaoDefinidoException();
        }
        try {
            const jogador = new Jogadorimpl();
            jogador.jogoFinalizou = JSON.parse(parameters.jogoFinalizouAtributo);
            jogador.jogada = parameters.jogadaAtributo !== undefined ?
                JSON.parse(parameters.jogadaAtributo) : undefined;
            jogador.esperoJogada = JSON.parse(parameters.esperoJogadaAtributo);
            return jogador;
        } catch (e) {
            if (e instanceof SyntaxError) {
                throw new JogadorInvalidoException();
            } else {
                throw e;
            }
        }
    }

    private static obterClienteDeRequisicao(req: Request, sessao: Sessao) {
        const clienteChaveString: string = String(req.params.cliente);
        const chaveCliente = new Chave();
        chaveCliente.associarValor(clienteChaveString);
        const cliente = sessao.encontrarClientePorChave(chaveCliente);
        if (cliente === undefined) {
            throw new ClienteNaoEncontradoException();
        }
        return cliente;
    }

    private static obterSessaoDeRequisicao(req: Request) {
        const sessaoChave: string = String(req.params.sessao);
        const chave = new Chave();
        chave.associarValor(sessaoChave);
        const sessao = sessoes.encontrarSessaoPorChave(chave);
        if (sessao === undefined) {
            throw new SessaoNaoEncontradaException();
        }
        return sessao;
    }

    private static statusPorProtocolo(protocolo: Protocolos): object {
        return {status: protocolo};
    }

}

// Rotas
router.get("/", RotaJogodavelha.servidor);
router.get("/novo-jogo", RotaJogodavelha.novoJogo);
router.get("/iniciar-jogo/:sessao", RotaJogodavelha.iniciarJogo);
router.get("/novo-jogo/:sessao", RotaJogodavelha.iniciarJogoPorJogadorAdversario);
router.post("/jogada/:sessao/:cliente", RotaJogodavelha.postarJogada);
router.get("/jogada/:sessao/:cliente", RotaJogodavelha.receberJogada);
router.get("/reiniciar-jogo/:sessao/:cliente", RotaJogodavelha.reiniciarJogo);
export default router;
