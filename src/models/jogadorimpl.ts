import Iinformacoes from "./iinformacoes";
import JogadorInvalidoException from "./jogadorInvalidoException";

class Jogadorimpl implements Iinformacoes {
    private jogadaAtributo: number | undefined;
    private jogoFinalizouAtributo: boolean;
    private esperoJogadaAtributo: boolean;

    constructor() {
        this.jogadaAtributo = 0;
        this.jogoFinalizouAtributo = false;
        this.esperoJogadaAtributo = false;

    }

    get esperoJogada(): boolean {
        return this.esperoJogadaAtributo;
    }

    set esperoJogada(value: boolean) {
        this.esperoJogadaAtributo = value;
    }

    get jogada(): number | undefined {
        return this.jogadaAtributo;
    }

    set jogada(value: number | undefined) {
        this.jogadaAtributo = value;
    }

    get jogoFinalizou(): boolean {
        return this.jogoFinalizouAtributo;
    }

    set jogoFinalizou(value: boolean) {
        this.jogoFinalizouAtributo = value;
    }

    public validarJogadorJogada(jogador: Jogadorimpl): void {
        if (jogador.esperoJogada !== this.esperoJogada) {
            throw new JogadorInvalidoException();
        }
        this.jogada = jogador.jogada;
        this.jogoFinalizou = jogador.jogoFinalizou;
        if (this.jogada === undefined || isNaN(this.jogada) || typeof(this.jogoFinalizou) !== typeof(true)) {
            throw new JogadorInvalidoException();
        }
    }

    public validarJogadorRecebimento(jogador: Jogadorimpl): void {
        if (jogador.esperoJogada !== this.esperoJogada) {
            throw new JogadorInvalidoException();
        }
    }
}

export default Jogadorimpl;
