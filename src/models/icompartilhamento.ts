import Chave from "./chave";
import Iinformacoes from "./iinformacoes";

interface ICompartilhamento {

    startCliente(chave: Chave): Iinformacoes;
}

export default ICompartilhamento;
