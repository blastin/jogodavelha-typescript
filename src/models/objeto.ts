import Jogadorimpl from "./jogadorimpl";

interface IObjeto {
    sessao: string;
    cliente: string;
    jogador: Jogadorimpl;
}

export default IObjeto;
