import {expect} from "chai";
import "mocha";
import Sessao from "../controls/sessao";
import Sessoes from "../controls/sessoes";
import Chave from "./chave";
import Cliente from "./cliente";
import JogadorEsperaJogadaException from "./JogadorEsperaJogadaException";
import Jogadorimpl from "./jogadorimpl";
import JogadorNaoEsperaJogadaException from "./JogadorNaoEsperaJogadaException";
import JogadorNaoPertenceAEsseJogoDaVelhaException from "./JogadorNaoPertenceAEsseJogoDaVelhaException";
import JogadorRealizouUltimaJogadaException from "./JogadorRealizouUltimaJogadaException";
import JogadorRecebeuUltimaJogadaException from "./JogadorRecebeuUltimaJogadaException";
import JogodaVelhaImpl from "./JogodaVelhaImpl";
import JogoFinalizouException from "./JogoFinalizouException";
import SemJogadaNovasException from "./SemJogadaNovasException";

describe("validar jogo da velha", () => {
    it("criar dois clientes e validar quem começa ", () => {
        const sessoes = new Sessoes();
        const jogodavelha: JogodaVelhaImpl = new JogodaVelhaImpl();
        const sessao = new Sessao(jogodavelha);
        sessoes.inserirSessao(sessao);
        const clienteA = new Cliente();
        const clienteB = new Cliente();
        sessao.inserirCliente(clienteA);
        sessao.inserirCliente(clienteB);
        const jogadorClienteA = clienteA.informacoes as Jogadorimpl;
        const jogadorClienteB = clienteB.informacoes as Jogadorimpl;
        expect(jogadorClienteA.esperoJogada === jogadorClienteB.esperoJogada).to.be.false;
    });
    it("validar que chave de cliente pertence ao jogo da velha", () => {
        const sessoes = new Sessoes();
        const jogodavelha: JogodaVelhaImpl = new JogodaVelhaImpl();
        const sessao = new Sessao(jogodavelha);
        sessoes.inserirSessao(sessao);
        const clienteA = new Cliente();
        const clienteB = new Cliente();
        sessao.inserirCliente(clienteA);
        sessao.inserirCliente(clienteB);
        const jogadorClienteA = clienteA.informacoes as Jogadorimpl;
        const jogadorClienteB = clienteB.informacoes as Jogadorimpl;
        expect(jogadorClienteA.esperoJogada === jogadorClienteB.esperoJogada).to.be.false;
        expect(jogodavelha.verificarSechaveDeClientePertenceAoJogo(clienteA.chave)).to.be.true;
        expect(jogodavelha.verificarSechaveDeClientePertenceAoJogo(clienteB.chave)).to.be.true;
        expect(jogodavelha.verificarSechaveDeClientePertenceAoJogo(new Cliente().chave)).to.be.false;
        const sessoesB = new Sessoes();
        const sessaoB = new Sessao(new JogodaVelhaImpl());
        const clienteC = new Cliente();
        sessoesB.inserirSessao(sessaoB);
        sessaoB.inserirCliente(clienteC);
        expect(jogodavelha.verificarSechaveDeClientePertenceAoJogo(clienteC.chave)).to.be.false;
    });
});

describe("trocando mensagens entre dois clientes, para uma poc de jogo", () => {
    it("jogador que pode realizar jogada publica", () => {
        const sessoes = new Sessoes();
        const jogodavelha: JogodaVelhaImpl = new JogodaVelhaImpl();
        const sessao = new Sessao(jogodavelha);
        const clienteA = new Cliente();
        const clienteB = new Cliente();
        sessoes.inserirSessao(sessao);
        sessao.inserirCliente(clienteA);
        sessao.inserirCliente(clienteB);
        const jogadorA = clienteA.informacoes as Jogadorimpl;
        const jogadorB = clienteB.informacoes as Jogadorimpl;
        const jogada = 100;
        if (!jogadorA.esperoJogada) {
            jogadorA.jogada = jogada;
            jogodavelha.publicarJogada(jogadorA, clienteA.chave);
        } else {
            jogadorB.jogada = jogada;
            jogodavelha.publicarJogada(jogadorB, clienteB.chave);
        }
        expect(jogodavelha.jogadaCompartilhada).equal(jogada);
    });
    it("jogador que pode realizar jogada publica e quando tenta novamente recebe exceção", () => {
        const sessoes = new Sessoes();
        const jogodavelha: JogodaVelhaImpl = new JogodaVelhaImpl();
        const sessao = new Sessao(jogodavelha);
        const clienteA = new Cliente();
        const clienteB = new Cliente();
        sessoes.inserirSessao(sessao);
        sessao.inserirCliente(clienteA);
        sessao.inserirCliente(clienteB);
        const jogadorA = clienteA.informacoes as Jogadorimpl;
        const jogadorB = clienteB.informacoes as Jogadorimpl;
        const jogada = 100;
        if (!jogadorA.esperoJogada) {
            jogadorA.jogada = jogada;
            jogodavelha.publicarJogada(jogadorA, clienteA.chave);
            expect(jogodavelha.jogadaCompartilhada).equal(jogada);
            expect(() => jogodavelha.publicarJogada(jogadorA, clienteA.chave))
                .to.throw(JogadorRealizouUltimaJogadaException);
        } else {
            jogadorB.jogada = jogada;
            jogodavelha.publicarJogada(jogadorB, clienteB.chave);
            expect(jogodavelha.jogadaCompartilhada).equal(jogada);
            expect(() => jogodavelha.publicarJogada(jogadorB, clienteB.chave))
                .to.throw(JogadorRealizouUltimaJogadaException);
        }
    });
    it("jogador que não pertence ao jogo da velha tenta publica uma jogada e recebe exceção", () => {
        const sessoes = new Sessoes();
        const jogodavelha: JogodaVelhaImpl = new JogodaVelhaImpl();
        const sessao = new Sessao(jogodavelha);
        const clienteA = new Cliente();
        const clienteB = new Cliente();
        sessoes.inserirSessao(sessao);
        sessao.inserirCliente(clienteA);
        sessao.inserirCliente(clienteB);
        const sessoesB = new Sessoes();
        const sessaoB = new Sessao(new JogodaVelhaImpl());
        const clienteC = new Cliente();
        sessoesB.inserirSessao(sessaoB);
        sessaoB.inserirCliente(clienteC);
        const jogadorC = clienteC.informacoes as Jogadorimpl;
        const jogada = 100;
        expect(() => jogodavelha.publicarJogada(jogadorC, clienteC.chave))
            .to.throw(JogadorNaoPertenceAEsseJogoDaVelhaException);
    });
    it("jogador tenta publicar mas jogo ja finalizou", () => {
        const sessoes = new Sessoes();
        const jogodavelha: JogodaVelhaImpl = new JogodaVelhaImpl();
        const sessao = new Sessao(jogodavelha);
        const clienteA = new Cliente();
        const clienteB = new Cliente();
        sessoes.inserirSessao(sessao);
        sessao.inserirCliente(clienteA);
        sessao.inserirCliente(clienteB);
        const jogadorA = clienteA.informacoes as Jogadorimpl;
        const jogadorB = clienteB.informacoes as Jogadorimpl;
        const jogada = 100;
        if (!jogadorA.esperoJogada) {
            jogadorA.jogoFinalizou = true;
            jogadorA.jogada = jogada;
            jogodavelha.publicarJogada(jogadorA, clienteA.chave);
            expect(jogodavelha.jogadaCompartilhada).equal(jogada);
            expect(() => jogodavelha.publicarJogada(jogadorA, clienteA.chave))
                .to.throw(JogoFinalizouException);
        } else {
            jogadorB.jogoFinalizou = true;
            jogadorB.jogada = jogada;
            jogodavelha.publicarJogada(jogadorB, clienteB.chave);
            expect(jogodavelha.jogadaCompartilhada).equal(jogada);
            expect(() => jogodavelha.publicarJogada(jogadorB, clienteB.chave))
                .to.throw(JogoFinalizouException);
        }
    });
    it("não é vez do jogador, ele tenta publicar e recebe exceção", () => {
        const sessoes = new Sessoes();
        const jogodavelha: JogodaVelhaImpl = new JogodaVelhaImpl();
        const sessao = new Sessao(jogodavelha);
        const clienteA = new Cliente();
        const clienteB = new Cliente();
        sessoes.inserirSessao(sessao);
        sessao.inserirCliente(clienteA);
        sessao.inserirCliente(clienteB);
        const jogadorA = clienteA.informacoes as Jogadorimpl;
        const jogadorB = clienteB.informacoes as Jogadorimpl;
        const jogada = 100;
        if (jogadorA.esperoJogada) {
            jogadorA.jogada = jogada;
            expect(() => jogodavelha.publicarJogada(jogadorA, clienteA.chave))
                .to.throw(JogadorEsperaJogadaException);
        } else {
            jogadorB.jogada = jogada;
            expect(() => jogodavelha.publicarJogada(jogadorB, clienteB.chave))
                .to.throw(JogadorEsperaJogadaException);
        }
    });
    it("jogador que estava em espera recebe jogada", () => {
        const sessoes = new Sessoes();
        const jogodavelha: JogodaVelhaImpl = new JogodaVelhaImpl();
        const sessao = new Sessao(jogodavelha);
        const clienteA = new Cliente();
        const clienteB = new Cliente();
        sessoes.inserirSessao(sessao);
        sessao.inserirCliente(clienteA);
        sessao.inserirCliente(clienteB);
        const jogadorA = clienteA.informacoes as Jogadorimpl;
        const jogadorB = clienteB.informacoes as Jogadorimpl;
        const jogada = 100;
        if (jogadorA.esperoJogada) {
            jogadorB.jogada = jogada;
            jogodavelha.publicarJogada(jogadorB, clienteB.chave);
            jogodavelha.receberJogada(jogadorA, clienteA.chave);
            expect(jogadorA.jogada).to.equal(jogada);
        } else {
            jogadorA.jogada = jogada;
            jogodavelha.publicarJogada(jogadorA, clienteA.chave);
            jogodavelha.receberJogada(jogadorB, clienteB.chave);
            expect(jogadorB.jogada).to.equal(jogada);
        }
        expect(jogodavelha.jogadaCompartilhada).to.equal(undefined);
    });
    it("jogador que não pertence ao jogo da velha tenta receber uma jogada e recebe exceção", () => {
        const sessoes = new Sessoes();
        const jogodavelha: JogodaVelhaImpl = new JogodaVelhaImpl();
        const sessao = new Sessao(jogodavelha);
        const clienteA = new Cliente();
        sessoes.inserirSessao(sessao);
        sessao.inserirCliente(clienteA);
        const sessoesB = new Sessoes();
        const sessaoB = new Sessao(new JogodaVelhaImpl());
        const clienteC = new Cliente();
        sessoesB.inserirSessao(sessaoB);
        sessaoB.inserirCliente(clienteC);
        const jogadorC = clienteC.informacoes as Jogadorimpl;
        expect(() => jogodavelha.receberJogada(jogadorC, clienteC.chave))
            .to.throw(JogadorNaoPertenceAEsseJogoDaVelhaException);
    });
    it("jogador tenta receber mas jogo ja finalizou", () => {
        const sessoes = new Sessoes();
        const jogodavelha: JogodaVelhaImpl = new JogodaVelhaImpl();
        const sessao = new Sessao(jogodavelha);
        const clienteA = new Cliente();
        const clienteB = new Cliente();
        sessoes.inserirSessao(sessao);
        sessao.inserirCliente(clienteA);
        sessao.inserirCliente(clienteB);
        const jogadorA = clienteA.informacoes as Jogadorimpl;
        const jogadorB = clienteB.informacoes as Jogadorimpl;
        const jogada = 100;
        if (!jogadorA.esperoJogada) {
            jogadorA.jogoFinalizou = true;
            jogadorA.jogada = jogada;
            jogodavelha.publicarJogada(jogadorA, clienteA.chave);
            expect(() => jogodavelha.receberJogada(jogadorA, clienteA.chave))
                .to.throw(JogoFinalizouException);
        } else {
            jogadorB.jogoFinalizou = true;
            jogadorB.jogada = jogada;
            jogodavelha.publicarJogada(jogadorB, clienteB.chave);
            expect(() => jogodavelha.receberJogada(jogadorB, clienteB.chave))
                .to.throw(JogoFinalizouException);
        }
    });
    it("jogador que estava em espera recebe jogada, tenta novamente e recebe exceção", () => {
        const sessoes = new Sessoes();
        const jogodavelha: JogodaVelhaImpl = new JogodaVelhaImpl();
        const sessao = new Sessao(jogodavelha);
        const clienteA = new Cliente();
        const clienteB = new Cliente();
        sessoes.inserirSessao(sessao);
        sessao.inserirCliente(clienteA);
        sessao.inserirCliente(clienteB);
        const jogadorA = clienteA.informacoes as Jogadorimpl;
        const jogadorB = clienteB.informacoes as Jogadorimpl;
        const jogada = 100;
        if (jogadorA.esperoJogada) {
            jogadorB.jogada = jogada;
            jogodavelha.publicarJogada(jogadorB, clienteB.chave);
            jogodavelha.receberJogada(jogadorA, clienteA.chave);
            expect(() => jogodavelha.receberJogada(jogadorA, clienteA.chave))
                .to.throw(JogadorRecebeuUltimaJogadaException);
        } else {
            jogadorA.jogada = jogada;
            jogodavelha.publicarJogada(jogadorA, clienteA.chave);
            jogodavelha.receberJogada(jogadorB, clienteB.chave);
            expect(() => jogodavelha.receberJogada(jogadorB, clienteB.chave))
                .to.throw(JogadorRecebeuUltimaJogadaException);
        }
    });
    it("jogador tenta receber jogada, mas ele deve publica. Recebe exceção", () => {
        const sessoes = new Sessoes();
        const jogodavelha: JogodaVelhaImpl = new JogodaVelhaImpl();
        const sessao = new Sessao(jogodavelha);
        const clienteA = new Cliente();
        const clienteB = new Cliente();
        sessoes.inserirSessao(sessao);
        sessao.inserirCliente(clienteA);
        sessao.inserirCliente(clienteB);
        const jogadorA = clienteA.informacoes as Jogadorimpl;
        const jogadorB = clienteB.informacoes as Jogadorimpl;
        if (!jogadorA.esperoJogada) {
            expect(() => jogodavelha.receberJogada(jogadorA, clienteA.chave))
                .to.throw(JogadorNaoEsperaJogadaException);
        } else {
            expect(() => jogodavelha.receberJogada(jogadorB, clienteB.chave))
                .to.throw(JogadorNaoEsperaJogadaException);
        }
    });
    it("jogador tenta receber jogada, mas sem jogada nova. Recebe exceção", () => {
        const sessoes = new Sessoes();
        const jogodavelha: JogodaVelhaImpl = new JogodaVelhaImpl();
        const sessao = new Sessao(jogodavelha);
        const clienteA = new Cliente();
        const clienteB = new Cliente();
        sessoes.inserirSessao(sessao);
        sessao.inserirCliente(clienteA);
        sessao.inserirCliente(clienteB);
        const jogadorA = clienteA.informacoes as Jogadorimpl;
        const jogadorB = clienteB.informacoes as Jogadorimpl;
        if (jogadorA.esperoJogada) {
            expect(() => jogodavelha.receberJogada(jogadorA, clienteA.chave))
                .to.throw(SemJogadaNovasException);
        } else {
            expect(() => jogodavelha.receberJogada(jogadorB, clienteB.chave))
                .to.throw(SemJogadaNovasException);
        }
    });

    it("jogador A e B, trocam 30 mensagens de um pseudo jogo", () => {
        const sessoes = new Sessoes();
        const jogodavelha: JogodaVelhaImpl = new JogodaVelhaImpl();
        const sessao = new Sessao(jogodavelha);
        const clienteA = new Cliente();
        const clienteB = new Cliente();
        sessoes.inserirSessao(sessao);
        sessao.inserirCliente(clienteA);
        sessao.inserirCliente(clienteB);
        const jogadorA = clienteA.informacoes as Jogadorimpl;
        const jogadorB = clienteB.informacoes as Jogadorimpl;
        let jogadorQueRealiza: Jogadorimpl;
        let jogadorQueRecebe: Jogadorimpl;
        let chaveJogadorQueRealiza: Chave;
        let chaveJogadorQueRecebe: Chave;
        if (jogadorA.esperoJogada) {
            jogadorQueRealiza = jogadorB;
            chaveJogadorQueRealiza = clienteB.chave;
            jogadorQueRecebe = jogadorA;
            chaveJogadorQueRecebe = clienteA.chave;
        } else {
            jogadorQueRealiza = jogadorA;
            chaveJogadorQueRealiza = clienteA.chave;
            jogadorQueRecebe = jogadorB;
            chaveJogadorQueRecebe = clienteB.chave;
        }
        let i = 0;
        while (i++ < 30) {
            const jogada = i;
            jogadorQueRealiza.jogada = jogada;
            jogodavelha.publicarJogada(jogadorQueRealiza, chaveJogadorQueRealiza);
            expect(jogodavelha.jogadaCompartilhada).to.equal(jogada);
            jogodavelha.receberJogada(jogadorQueRecebe, chaveJogadorQueRecebe);
            expect(jogodavelha.jogadaCompartilhada).to.equal(undefined);
            expect(jogadorQueRecebe.jogada).to.equal(jogada);
            [jogadorQueRealiza, jogadorQueRecebe] = [jogadorQueRecebe, jogadorQueRealiza];
            [chaveJogadorQueRealiza, chaveJogadorQueRecebe] = [chaveJogadorQueRecebe, chaveJogadorQueRealiza];
        }
    });
});
