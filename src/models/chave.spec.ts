import {expect} from "chai";
import "mocha";
import AssociacaoDeValorEmChaveDefinidaException from "./associacaoDeValorEmChaveDefinidaException";
import AssociacaoDeValorIndefinidoException from "./associacaoDeValorIndefinidoException";
import Chave from "./chave";

describe("criar chave", () => {
    it("chave gerada", () => {
        const chave = new Chave();
        chave.gerarChave();
        const exp = expect(chave.chaveGerada()).to.be.true;
    });
});

describe("associar valor em chave", () => {
    it("deve associar um valor", () => {
        const chave = new Chave();
        const valor = "34834284328";
        chave.associarValor(valor);
        const exp = expect(chave.chaveGerada()).to.be.true;
        expect(chave.toString()).to.equal(valor);
    });
    it("associar um valor indefinido", () => {
        const chave = new Chave();
        const valor = "";
        expect(() => chave.associarValor(valor)).to.throw(AssociacaoDeValorIndefinidoException);
    });
});

describe("associar valor em chave com valor e gerar exceção", () => {
    it("deve gerar uma excessao que chave ja foi gerada", () => {
        const valor = "34834284328";
        const chave = new Chave();
        chave.gerarChave();
        expect(() => chave.associarValor(valor)).to.throw(AssociacaoDeValorEmChaveDefinidaException);
    });
});
