import {expect} from "chai";
import "mocha";
import Sorteio from "./Sorteio";

describe("sorteiar um valor", () => {
    it("deve sortear um valor de 0 a 1", () => {
        const sorteio = Sorteio.sortear(0, 1);
        expect(sorteio).to.gte(0).to.lte(1);
    });
    it("deve sortear um valor entre 10 a 40", () => {
        const sorteio = Sorteio.sortear(10, 40);
        expect(sorteio).to.gte(10).to.lte(40);
    });
});
