import AssociacaoDeValorEmChaveDefinidaException from "./associacaoDeValorEmChaveDefinidaException";
import AssociacaoDeValorIndefinidoException from "./associacaoDeValorIndefinidoException";
import Sorteio from "./Sorteio";

class Chave {

    private static cadeia: string[] = [];

    private static iniciarCadeia() {
        if (Chave.cadeia.length === 0) {
            for (let i = 48; i <= 57; i++) {
                Chave.cadeia.push(String.fromCharCode(i));
            }
            for (let i = 65; i <= 90; i++) {
                Chave.cadeia.push(String.fromCharCode(i));
            }
            for (let i = 97; i <= 122; i++) {
                Chave.cadeia.push(String.fromCharCode(i));
            }
        }
    }

    private valor: string | undefined;

    constructor() {
        this.valor = undefined;
        Chave.iniciarCadeia();
    }

    public associarValor(valor: string | undefined) {
        if (this.chaveGerada()) {
            throw new AssociacaoDeValorEmChaveDefinidaException();
        }
        if (valor === undefined || valor.length === 0) {
            throw new AssociacaoDeValorIndefinidoException();
        }
        this.valor = valor;
    }

    public gerarChave(): void {
        const sorteio = Sorteio.sortear(30, 40);
        this.valor = "";
        for (let i = 0; i < sorteio; i++) {
            this.valor += Chave.cadeia[Sorteio.sortear(0, Chave.cadeia.length - 1)];
        }
    }

    public chaveGerada(): boolean {
        return this.valor !== undefined;
    }

    public toString(): string {
        return String(this.valor);
    }

    public equal(chave: Chave | undefined): boolean {
        if (chave === undefined) {
            return false;
        }
        return this.valor === chave.valor;
    }

}

export default Chave;
