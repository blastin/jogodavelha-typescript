import Chave from "./chave";
import ICompartilhamento from "./icompartilhamento";
import Iinformacoes from "./iinformacoes";
import IJogador from "./IJogador";
import JogadorEsperaJogadaException from "./JogadorEsperaJogadaException";
import Jogadorimpl from "./jogadorimpl";
import JogadorNaoEsperaJogadaException from "./JogadorNaoEsperaJogadaException";
import JogadorNaoPertenceAEsseJogoDaVelhaException from "./JogadorNaoPertenceAEsseJogoDaVelhaException";
import JogadorRealizouUltimaJogadaException from "./JogadorRealizouUltimaJogadaException";
import JogadorRecebeuUltimaJogadaException from "./JogadorRecebeuUltimaJogadaException";
import JogoDaVelhaErrorInternoJogadaNaoDeveriaExistirException
    from "./JogoDaVelhaErrorInternoJogadaNaoDeveriaExistirException";
import JogoFinalizouException from "./JogoFinalizouException";
import JogoNaoFinalizouException from "./JogoNaoFinalizouException";
import QuantidadeDeJogadoresMaiorDoQuePermitidoException from "./QuantidadeDeJogadoresMaiorDoQuePermitidoException";
import SemJogadaNovasException from "./SemJogadaNovasException";
import Sorteio from "./Sorteio";

class JogodaVelhaImpl implements ICompartilhamento {

    private readonly quantidadeJogadoresPermitidos: number;
    private jogadaCompartilhaAtt: number | undefined;
    private jogadores: IJogador[];
    private sorteio: number;
    private chaveDeUltimaPublicacao: Chave | undefined;
    private chaveDeUltimoRecebimento: Chave | undefined;
    private jogoFinalizou: boolean;

    constructor() {
        this.quantidadeJogadoresPermitidos = 2;
        this.jogadores = [];
        this.sorteio = 0;
        this.jogoFinalizou = false;
        this.novoJogoInicial();
    }

    get jogadaCompartilhada(): number | undefined {
        return this.jogadaCompartilhaAtt;
    }

    set jogadaCompartilhada(value: number | undefined) {
        this.jogadaCompartilhaAtt = value;
    }

    public startCliente(chave: Chave): Iinformacoes {
        if (this.jogadores.length === this.quantidadeJogadoresPermitidos) {
            throw new QuantidadeDeJogadoresMaiorDoQuePermitidoException();
        }
        const jogador: Jogadorimpl = new Jogadorimpl();
        const object: IJogador = {
            chave: chave.toString(),
        };
        this.jogadores.push(object);
        if (this.sorteio !== this.jogadores.length) {
            jogador.esperoJogada = true;
        }

        return jogador;
    }

    public publicarJogada(jogador: Jogadorimpl, chave: Chave): void {
        if (!this.verificarSechaveDeClientePertenceAoJogo(chave)) {
            throw new JogadorNaoPertenceAEsseJogoDaVelhaException();
        }
        if (this.jogoFinalizou) {
            throw new JogoFinalizouException();
        }
        if (chave.equal(this.chaveDeUltimaPublicacao)) {
            throw new JogadorRealizouUltimaJogadaException();
        }
        if (jogador.esperoJogada) {
            throw new JogadorEsperaJogadaException();
        }
        if (this.jogadaCompartilhada !== undefined) {
            throw new JogoDaVelhaErrorInternoJogadaNaoDeveriaExistirException(
                "ERROR: Não deveria exister jogada definida se um jogador não espera jogada");
        }
        if (jogador.jogoFinalizou) {
            this.jogoFinalizou = true;
        } else {
            jogador.esperoJogada = true;
        }
        this.jogadaCompartilhaAtt = jogador.jogada;
        this.chaveDeUltimaPublicacao = chave;
    }

    public receberJogada(jogador: Jogadorimpl, chave: Chave): void {
        if (!this.verificarSechaveDeClientePertenceAoJogo(chave)) {
            throw new JogadorNaoPertenceAEsseJogoDaVelhaException();
        }
        if (this.jogoFinalizou) {
            jogador.jogada = this.jogadaCompartilhada;
            throw new JogoFinalizouException();
        }
        if (chave.equal(this.chaveDeUltimoRecebimento) === true) {
            throw new JogadorRecebeuUltimaJogadaException();
        }
        if (chave.equal(this.chaveDeUltimaPublicacao) === true) {
            throw new JogadorRealizouUltimaJogadaException();
        }
        if (!jogador.esperoJogada) {
            throw new JogadorNaoEsperaJogadaException();
        }
        if (this.jogadaCompartilhada === undefined) {
            throw new SemJogadaNovasException();
        }
        jogador.jogada = this.jogadaCompartilhada;
        jogador.esperoJogada = false;
        this.jogadaCompartilhada = undefined;
        this.chaveDeUltimoRecebimento = chave;
    }

    public reiniciarJogo(jogador: Jogadorimpl, chave: Chave) {
        const iJogador: any = this.retornarIJogadorPorChave(chave);
        if (iJogador === undefined) {
            throw new JogadorNaoPertenceAEsseJogoDaVelhaException();
        }
        console.log(this);
        if (!this.jogoFinalizou) {
            throw new JogoNaoFinalizouException();
        }
        const index = this.jogadores.indexOf(iJogador as IJogador);
        this.novoJogoInicial();
        jogador.esperoJogada = index !== this.sorteio;
        jogador.jogoFinalizou = false;
        jogador.jogada = 0;
    }

    private retornarIJogadorPorChave(chave: Chave): any {
        return this.jogadores.find((object) => object.chave === chave.toString());
    }

    private verificarSechaveDeClientePertenceAoJogo(chave: Chave): boolean {
        return this.retornarIJogadorPorChave(chave) !== undefined;
    }

    private novoJogoInicial() {
        this.jogadaCompartilhaAtt = undefined;
        this.sorteio = Sorteio.sortear(1, 2);
        this.chaveDeUltimaPublicacao = undefined;
        this.chaveDeUltimoRecebimento = undefined;
        this.jogoFinalizou = false;
    }

}

export default JogodaVelhaImpl;
