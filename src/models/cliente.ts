import RepositorioChave from "../controls/repositorio.chave";
import Chave from "./chave";
import ICompartilhamento from "./icompartilhamento";
import Iinformacoes from "./iinformacoes";

class Cliente {

    private repositorioChaves: RepositorioChave;
    private compartilhamentoPorSessaoAtt: ICompartilhamento | undefined;
    private readonly chaveAtt: Chave;
    private informacoesAtt: Iinformacoes | undefined;

    constructor() {
        this.chaveAtt = new Chave();
        this.repositorioChaves = new RepositorioChave();
        this.compartilhamentoPorSessaoAtt = undefined;
        this.informacoesAtt = undefined;
    }

    public get chave() {
        return this.chaveAtt;
    }

    public gerarChaveDeCliente(): void {
        this.chaveAtt.gerarChave();
    }

    public get informacoes(): Iinformacoes | undefined {
        return this.informacoesAtt;
    }

    public start(compartilhamentoPorSessao: ICompartilhamento): void {
        this.compartilhamentoPorSessaoAtt = compartilhamentoPorSessao;
        this.informacoesAtt = this.compartilhamentoPorSessaoAtt.startCliente(this.chave);
    }

}

export default Cliente;
