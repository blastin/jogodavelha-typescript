// Express
import cookieParser from "cookie-parser";
import dotenv from "dotenv";
import renderFile from "ejs";
import express from "express";
import logger from "morgan";
import path from "path";
import router from "./src/routes/rota.jogodavelha";

const application = express();
dotenv.config();
// Aplicação
application.use(logger("dev"));
application.use(express.json());
application.use(express.urlencoded({
    extended: false,
}));
application.use("/", router);

application.use(cookieParser());
application.use(express.static(path.join(__dirname, "public")));
application.set("views", path.join(__dirname, "/public"));
application.engine("html", renderFile.renderFile);
application.set("view engine", "ejs");
// Escuta
application.listen(process.env.PORT, () => {
    if (process.env.USER_GMAIL === undefined) {
        console.log("Error: arquivo .env deve conter USER_GMAIL=\"EMAIL_LOG@GMAIL.COM\"");
    }
    if (process.env.PASSWORD_GMAIL === undefined) {
        console.log("Error: arquivo .env deve conter PASSWORD_GMAIL=\"PASSWORD EMAIL\"");
    }
    console.log("Iniciando servidor básico na porta<" + process.env.PORT + "> . Serviço online");
});
